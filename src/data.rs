mod raw;

use rand::{seq::IteratorRandom, seq::SliceRandom};
use raw::Relationship::*;
use serde::Serialize;
use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    fs::File,
    hash::Hash,
    io::prelude::*,
    io::BufReader,
    iter::FromIterator,
};

#[derive(Debug, Eq, PartialEq, Hash, Clone, Serialize)]
struct Language(String);
#[derive(Debug, Eq, PartialEq, Hash, Clone,  Serialize)]
pub struct Word {
    language: Language,
    pub word: String,
}

impl<'a> From<&raw::Word<'a>> for Word {
    fn from(r: &raw::Word) -> Self {
        Self {
            language: Language(r.language.0.to_string()),
            word: r.word.to_string(),
        }
    }
}

impl Word {
    pub fn origns<'a>(&self, data: &'a Data) -> Option<&'a Self> {
        match data.words.get(self) {
            Some(x) => {
                let filtered = x.into_iter().filter(|x| self.language != x.language);
                filtered.choose(&mut rand::thread_rng())
            }
            None => None,
        }
    }
}

fn push_at<K, V>(map: &mut HashMap<K, Vec<V>>, key: K, value: V)
where
    K: Hash,
    K: Eq,
{
    let e = map.entry(key);
    match e {
        Entry::Occupied(mut e) => e.get_mut().push(value),
        Entry::Vacant(e) => {
            e.insert(vec![value]);
        }
    }
}

pub struct Data {
    words: HashMap<Word, Vec<Word>>,
    english_words: Vec<Word>,
}
impl Data {
    pub fn init() -> Data {
        let file = File::open("data/etymwn.tsv").unwrap();
        let buf_reader = BufReader::new(file);

        let mut words: HashMap<Word, Vec<Word>> = HashMap::new();
        let mut english_words: HashSet<Word> = HashSet::new();

        for line in buf_reader.lines().map(Result::unwrap) {
            let line = (raw::EtymologicalRelationShip::new(&line)).unwrap();
            if line.from.language.0 == "eng" {
                english_words.insert((&line.from).into());
            }
            match line {
                raw::EtymologicalRelationShip {
                    from,
                    relationship: HasDerivedForm,
                    to,
                }
                | raw::EtymologicalRelationShip {
                    from: to,
                    relationship: EtymologicalOriginOf,
                    to: from,
                } => {
                    push_at(&mut words, (&from).into(), (&to).into());
                }
                raw::EtymologicalRelationShip {
                    from,
                    relationship: _,
                    to,
                } => {
                    push_at(&mut words, (&from).into(), (&to).into());
                    push_at(&mut words, (&to).into(), (&from).into());
                }
            };
        }

        Data {
            words,
            english_words: Vec::from_iter(english_words),
        }
    }
    pub fn random_english_word(&self) -> &Word {
        self.english_words.choose(&mut rand::thread_rng()).unwrap()
    }
}
