mod data;

use data::{Data, Word};
use lazy_static::lazy_static;
use serde::Serialize;
use tide::{Request, Response, StatusCode};

lazy_static! {
    static ref DATA: Data = Data::init();
}

#[derive(Serialize, Debug)]
struct EtymologyTree {
    word: Word,
    originates_from: Option<Box<EtymologyTree>>,
}

impl EtymologyTree {
    fn of(word: &Word, data: &Data, mut used: Vec<Word>) -> Self {
        EtymologyTree {
            word: (*word).clone(),
            originates_from: word.origns(data).and_then(|x| {
                if used.contains(word) {
                    None
                } else {
                    Some(Box::new(EtymologyTree::of(x, data, {
                        used.push(word.clone());
                        used
                    })))
                }
            }),
        }
    }
    fn for_random_word() -> Self {
        loop {
            let random_word = (&*DATA).random_english_word();
            let tree = EtymologyTree::of(random_word, &*DATA, Vec::new());
            if tree.originates_from.is_some() {
                break tree;
            }
        }
    }
}

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    let mut app = tide::new();
    app.at("/").get(|_| async {
        Ok(Response::new(StatusCode::Ok).body_json(&EtymologyTree::for_random_word())?)
    });
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}
